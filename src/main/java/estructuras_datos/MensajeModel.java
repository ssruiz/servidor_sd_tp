/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras_datos;

/**
 *
 * @author sam
 */
public class MensajeModel {

	private int estado;
	private int tipoOperacion;
	private String mensaje;
	private String cuerpo;

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public int getTipoOperacion() {
		return tipoOperacion;
	}

	public void setTipoOperacion(int tipoOperacion) {
		this.tipoOperacion = tipoOperacion;
	}

	public String getCuerpo() {
		return cuerpo;
	}

	public void setCuerpo(String cuerpo) {
		this.cuerpo = cuerpo;
	}

	@Override
	public String toString() {
		return "MensajeModel [estado=" + estado + ", tipoOperacion=" + tipoOperacion + ", mensaje=" + mensaje
				+ ", cuerpo=" + cuerpo.toString() + "]";
	}
}