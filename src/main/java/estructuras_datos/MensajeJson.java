/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package estructuras_datos;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sam
 */
public class MensajeJson {

//    public static void main(String[] args) throws Exception {
//        MensajeJson representacion = new MensajeJson();
//        MensajeModel m = new MensajeModel();
//        System.out.println("Ejemplo de uso 1: pasar de objeto a string");
//        CuerpoMensaje p = new CuerpoMensaje();
//        m.setEstado(1);
//        m.setMensaje("ok");
//        m.setTipoOperacion(2);
//
//        p.setMensaje("Mensaje del cuerpo");
//        p.setTipoMensaje(35);
//        m.setCuerpo(p);
//
//        String r1 = representacion.objetoString(m);
//        System.out.println(r1);
//
//        System.out.println("\n*************************************************************************");
//        System.out.println("\nEjemplo de uso 2: pasar de string a objeto");
//        String un_string = "{\"cedula\":123123,\"nombre\":\"Ana\",\"apellido\":\"Perez\",\"asignaturas\":[\"Sistemas Distribuidos\",\"Fisica\",\"Inteligencia Artificial\"]}";
//
//        MensajeModel r2 = representacion.stringObjeto(r1);
//        System.out.println(r2.toString());
//
//    }

    public String objetoString(MensajeModel m) {

        JSONObject obj = new JSONObject();
        JSONObject obj2 = new JSONObject();
        obj.put("estado", m.getEstado());
        obj.put("mensaje", m.getMensaje());
        obj.put("tipoOperacion", m.getTipoOperacion());

        String cuerpo = m.getCuerpo();
//        obj2.put("mensaje", cuerpo.getMensaje());
//        obj2.put("tipo", cuerpo.getTipoMensaje());
//        obj2.put("nombreCliente", cuerpo.getNombreCliente());
//        obj2.put("respuesta", cuerpo.getRespuesta());
        // JSONArray list = new JSONArray();
        // list.add("mensaje",cuerpo.getMensaje());
        // if(list.size() > 0) {
        obj.put("cuerpo", cuerpo);
        // }

        return obj.toJSONString();
    }

    public MensajeModel stringObjeto(String str) throws Exception {
        MensajeModel p = new MensajeModel();
        JSONParser parser = new JSONParser();

        Object obj = parser.parse(str.trim());
        JSONObject jsonObject = (JSONObject) obj;

        Long aux = (Long) jsonObject.get("estado");
        p.setEstado(aux.intValue());
        p.setMensaje((String) jsonObject.get("mensaje"));
        aux = (Long) jsonObject.get("tipoOperacion");
        p.setTipoOperacion(aux.intValue());

        String cuerpo = (String) jsonObject.get("cuerpo");
        p.setCuerpo(cuerpo);
//		cuerpo.setMensaje(msg.get(0));
//
//		Iterator<String> iterator = msg.iterator();
//		while (iterator.hasNext()) {
//			p.getAsignaturas().add(iterator.next());
//		}
        return p;
    }
}
