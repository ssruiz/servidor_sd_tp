/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servidor_sd_tp;

import org.apache.log4j.Logger;

/**
 *
 * @author sam
 */
public class ServidorMain {

    private static Logger logger = Logger.getLogger(ServidorMain.class);

    public static void main(String[] args) {
        Servidor s = new Servidor();
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                try {
                    System.out.println("Cerrando Servidor");
                    s.serverSocket.close();
                    s.socket.close();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // s.start();
    }

}
