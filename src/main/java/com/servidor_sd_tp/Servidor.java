/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servidor_sd_tp;

import controllers.MensajeController;
import java.io.IOException;
import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

/**
 *
 * @author sam
 */
public class Servidor extends Thread {

    private static Logger logger = Logger.getLogger(Servidor.class);
    private ServerSocket servidor = null;
    private MensajeController controller;
    // protected List<ClienteHilo> listadoClientes;

    protected Map<Integer, ClienteHilo> listadoClientes = null;
    int puerto = 4444;
    int codigoCliente = 1;
    DatagramSocket serverSocket;
    Socket socket = null;

    //int maximoConexiones = 10; // Maximo de conexiones simultaneas
    public Servidor() {
        try {
            // Se crea el serverSocket
            servidor = new ServerSocket(puerto);
            PropertyConfigurator.configure("log4j.properties");

            listadoClientes = Collections.synchronizedMap(new HashMap<Integer, ClienteHilo>());
            //inicia el hilo
            logger.info("Servidor a la espera de conexiones en el puerto: " + puerto);
            this.start();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void run() {
        socket = null;
        try {
            while (true) {
                socket = servidor.accept();
                logger.info("Cliente con la IP " + socket.getInetAddress().getHostName() + " conectado al puerto: " + socket.getPort() + "");
                ClienteHilo nuevoCliente = new ClienteHilo(socket, listadoClientes, codigoCliente);        
                listadoClientes.put(codigoCliente, nuevoCliente);
                codigoCliente++;       
                Thread hilo = new Thread(nuevoCliente);
                hilo.start();            
            }
        } catch (IOException ex) {
            logger.error("Error: " + ex.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {
                socket.close();
                servidor.close();
            } catch (IOException ex) {
                logger.error("Error al cerrar el servidor: " + ex.getMessage());
            }
        }
    }

    /**/
    public void shutdown() {
        try {
            servidor.close();
            logger.info("Sokect cerrado correctamente");
            socket.close();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
