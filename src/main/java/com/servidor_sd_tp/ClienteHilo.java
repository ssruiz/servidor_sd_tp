/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.servidor_sd_tp;

import controllers.MensajeController;
import controllers.RespuestaMensaje;
import estructuras_datos.MensajeJson;
import estructuras_datos.MensajeModel;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.util.Map;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;

/**
 * Clase que permite al servidor ser multihilo. Actua de servidor para el
 * cliente
 *
 * @author Samuel Ruiz
 */
public class ClienteHilo implements Runnable {

    Socket socket;

    /*      ----------- Datos del cliente ------------------*/
    int codigoCliente;
    String nombreCliente = null;
    boolean enLlamada;
    /* -------- */

    DataInputStream socketEntrada;
    DataOutputStream socketSalida;

    private static Logger logger;

    MensajeController mc;

    /* Listado de usuarios guardado en HasMap donde el key es generado por el servidor y el value es el hilo del cliente */
    Map<Integer, ClienteHilo> listadoClientes = null;

    /**
     * Constructor del clase
     *
     * @param origen Socket creado para el cliente
     * @param l Listado de clientes manejados por el servidor
     * @param cod Identificador para el cliente nuevo
     */
    public ClienteHilo(Socket origen, Map<Integer, ClienteHilo> l, int cod) {
        try {
            logger = Logger.getLogger(ClienteHilo.class);
            mc = new MensajeController();
            listadoClientes = l;
            BasicConfigurator.configure();
            this.socket = origen;
            enLlamada = false;
            codigoCliente = cod;
            nombreCliente = "";
            socketEntrada = new DataInputStream(socket.getInputStream());
            socketSalida = new DataOutputStream(socket.getOutputStream());
            //socketEntrada = new DataInputStream(socket.getInputStream());
            //socketSalida = new DataOutputStream(socket.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Se inicia el hilo
     */
    public void run() {
        try {
            MensajeController messageController = new MensajeController();
            MensajeJson mj = new MensajeJson();
            MensajeModel mensajeM = new MensajeModel();
            mensajeM = mj.stringObjeto(socketEntrada.readUTF());
            while (true) {
                if (mc.clienteDuplicado(mensajeM.getCuerpo(), listadoClientes, mensajeM)) {

                    socketSalida.writeUTF(mj.objetoString(mensajeM));
                } else {
                    mensajeM.setEstado(RespuestaMensaje.NO_ERROR.getLevelCode());
                    mensajeM.setMensaje("Login exitoso");
                    socketSalida.writeUTF(mj.objetoString(mensajeM));
                    break;
                }
                mensajeM = mj.stringObjeto(socketEntrada.readUTF());
            }

            ClienteHilo ca = listadoClientes.get(codigoCliente);
            ca.nombreCliente = mensajeM.getCuerpo();
            messageController.registrarActividad(ca.socket.getInetAddress() + ":" + ca.socket.getPort(), "--", 0);

            synchronized (listadoClientes) {
                for (Map.Entry<Integer, ClienteHilo> entry : listadoClientes.entrySet()) {
                    // System.out.println(entry.getKey() + " = " + entry.getValue().toString());
                }
            }

            String readline;
            boolean continuar = true;

            int i = 0;

            while (continuar) {
                logger.info("ESPERANDO PETICION");
                readline = socketEntrada.readUTF();
                ClienteHilo clienteDestino;
                MensajeModel mm = mj.stringObjeto(readline);
                String response = "";
                switch (mm.getTipoOperacion()) {
                    case 1:
                        mc = new MensajeController();
                        response = mc.getListadoClientes(listadoClientes);
                        //System.out.println(response);
                        messageController.registrarActividad(getSocket().getInetAddress() + ":" + getSocket().getPort(), "-", 1);
                        socketSalida.writeUTF(response);
                        break;
                    case 2:
                        // ClienteHilo ch1 = mc.getCliente(mm.getCuerpo(), listadoClientes);
                        clienteDestino = mc.getCliente(mm.getCuerpo(), listadoClientes);
                        if (clienteDestino != null) {
                            if (!clienteDestino.isEnLlamada()) {
                                clienteDestino.setEnLlamada(true);
                                this.setEnLlamada(true);

                                DataOutputStream auxOut = clienteDestino.getSocketSalida();
                                mm.setEstado(0);
                                auxOut.writeUTF(mj.objetoString(mm));
                                messageController.registrarActividad(getSocket().getInetAddress() + ":" + getSocket().getPort(),
                                        clienteDestino.getSocket().getInetAddress() + ":" + clienteDestino.getSocket().getPort(), 2);
                                socketSalida.writeUTF(mj.objetoString(mm));

                            } else {
                                mm.setEstado(RespuestaMensaje.USER_IN_CALL.getLevelCode());
                                mm.setMensaje("Usuario ocupado. Actualmente en llamada");
                                socketSalida.writeUTF(mj.objetoString(mm));
                            }

                        } else {
                            mm.setEstado(RespuestaMensaje.USER_NOT_FOUND.getLevelCode());
                            mm.setMensaje("Usuario no encontrado");
                            socketSalida.writeUTF(mj.objetoString(mm));
                        }
                        break;
                    // llamada rechazada    
                    case 14:
                        ClienteHilo hiloLlamador = mc.getCliente(mm.getCuerpo(), listadoClientes);
                        hiloLlamador.setEnLlamada(false);
                        this.setEnLlamada(false);
                        DataOutputStream streamHiloLlamador = hiloLlamador.getSocketSalida();
                        mm.setEstado(0);
                        mm.setMensaje("Llamada Rechazada por" + this.getNombreCliente());
                        streamHiloLlamador.writeUTF(mj.objetoString(mm));
                        //socketSalida.writeUTF(mj.objetoString(mm));
                        break;
                    //Acepta llamada    
                    case 15:
                        ClienteHilo hiloReceptor = mc.getCliente(mm.getCuerpo(), listadoClientes);
                        DataOutputStream sr = hiloReceptor.getSocketSalida();
                        sr.writeUTF(readline);
                        break;
                    //Terminar llamada
                    case 16:
                        ClienteHilo hiloTerminar = mc.getCliente(mm.getCuerpo(), listadoClientes);
                        hiloTerminar.setEnLlamada(false);
                        this.setEnLlamada(false);
                        DataOutputStream streamTerminarLlamada = hiloTerminar.getSocketSalida();
                        mm.setEstado(0);
                        mm.setMensaje("Llamada finalizada por: " + this.getNombreCliente());
                        streamTerminarLlamada.writeUTF(mj.objetoString(mm));
                        socketSalida.writeUTF(mj.objetoString(mm));
                        break;
                    case 3:

                        ClienteHilo ch2 = mc.getCliente(mm.getCuerpo(), listadoClientes);

                        if (ch2 != null) {
                            DataOutputStream auxOut = ch2.getSocketSalida();
                            auxOut.writeUTF(mc.getMsgUDP(mm.getCuerpo(), nombreCliente));
                            messageController.registrarActividad(getSocket().getInetAddress() + ":" + getSocket().getPort(),
                                    ch2.getSocket().getInetAddress() + ":" + ch2.getSocket().getPort(), 3);
                        } else {
                            mm.setEstado(RespuestaMensaje.USER_NOT_FOUND.getLevelCode());
                            mm.setMensaje("Usuario no encontrado");
                            socketSalida.writeUTF(mj.objetoString(mm));
                        }
                        break;
                    case 4:
                        listadoClientes.remove(codigoCliente);
                        response = mc.getMsgDisconnect();
                        
                        messageController.registrarActividad(getSocket().getInetAddress() + ":" + getSocket().getPort(), "--", 4);

                        socketSalida.writeUTF(response);
                        continuar = false;
                        break;
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            try {

                socketEntrada.close();
                socketSalida.close();
                socket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

    }

    /**
     * Getter del socket
     *
     * @return Referencia al socket del cliente
     */
    public Socket getSocket() {
        return socket;
    }

    /**
     * Setter del Socket
     *
     * @param socket Referencia al socket del cliente
     */
    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    /**
     * Getter
     *
     * @return
     */
    public int getCodigoCliente() {
        return codigoCliente;
    }

    public void setCodigoCliente(int codigoCliente) {
        this.codigoCliente = codigoCliente;
    }

    public DataInputStream getSocketEntrada() {
        return socketEntrada;
    }

    public void setSocketEntrada(DataInputStream socketEntrada) {
        this.socketEntrada = socketEntrada;
    }

    public DataOutputStream getSocketSalida() {
        return socketSalida;
    }

    public void setSocketSalida(DataOutputStream socketSalida) {
        this.socketSalida = socketSalida;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public boolean isEnLlamada() {
        return enLlamada;
    }

    /* --------------- Variables para lectura y escritura ----------- */
    public void setEnLlamada(boolean enLlamada) {
        this.enLlamada = enLlamada;
    }

    @Override
    public String toString() {
        return "ClienteHilo{" + "origen=" + socket.getPort() + "} Usuario: " + nombreCliente + "";
    }

}
