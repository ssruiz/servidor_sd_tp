/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

/**
 *
 * @author sam
 */
public enum RespuestaMensaje {
    UNDEFINED(1),
    NO_ERROR(0),
    USER_DUPLICATE(2),
    USER_NOT_FOUND(3),
    USER_IN_CALL(4);
    private final int levelCode;

    private RespuestaMensaje(int levelCode) {
        this.levelCode = levelCode;
    }
    
     public int getLevelCode() {
        return this.levelCode;
    }
    
}
