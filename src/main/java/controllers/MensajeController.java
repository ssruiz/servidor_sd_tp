/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import com.servidor_sd_tp.ClienteHilo;
import db.LogDAO;
import db.LogModel;
import estructuras_datos.CuerpoMensaje;
import estructuras_datos.MensajeJson;
import estructuras_datos.MensajeModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.hamcrest.core.IsNull;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author sam
 */
public class MensajeController {

    private String retornaLista(Map<Integer, ClienteHilo> listadoClientes) {

        JSONArray list = new JSONArray();
        for (Map.Entry<Integer, ClienteHilo> entry : listadoClientes.entrySet()) {
            String user = entry.getValue().getNombreCliente();
            list.add(user);
        }
        return list.toJSONString();
    }

    public String getListadoClientes(Map<Integer, ClienteHilo> listadoClientes) {
        String result = "";
        MensajeModel mm = new MensajeModel();
        CuerpoMensaje mc = new CuerpoMensaje();
        MensajeJson mj = new MensajeJson();
        mm.setEstado(0);
        mm.setMensaje("ok");
        mm.setTipoOperacion(1);
        //mc.setMensaje("username");
        //mc.setTipoMensaje(1);
        //mc.setNombreCliente("");
        //mc.setRespuesta(retornaLista(listadoClientes));
        mm.setCuerpo(retornaLista(listadoClientes));
        return mj.objetoString(mm);
    }

    public boolean clienteDuplicado(String user, Map<Integer, ClienteHilo> listadoClientes, MensajeModel mm) {
        boolean result = false;

        if (listadoClientes == null) {
            return false;
        }
        if (listadoClientes.isEmpty()) {
            return false;
        }
        for (Map.Entry<Integer, ClienteHilo> entry : listadoClientes.entrySet()) {
            if (entry.getValue().getNombreCliente().equals(user)) {
                result = true;
                mm.setEstado(RespuestaMensaje.USER_DUPLICATE.getLevelCode());
                mm.setMensaje("Nombre de usuario ya en uso");
                break;
            }
        }
        return result;

    }

//    private String prepararMensaje(int op) {
//        MensajeModel mm = new MensajeModel();
//        CuerpoMensaje mc = new CuerpoMensaje();
//        MensajeJson mj = new MensajeJson();
//        switch (op) {
//            case 0:
//                mm.setEstado(-1);
//                mm.setMensaje("ok");
//                mm.setTipoOperacion(0);
//                mc.setMensaje("username");
//                mc.setTipoMensaje(1);
//                mc.setNombreCliente(username);
//                mm.setCuerpo(mc);
//                return mj.objetoString(mm);
//            case 1:
//                mm.setEstado(-1);
//                mm.setMensaje("ok");
//                mm.setTipoOperacion(1);
//                mc.setMensaje("Get listado");
//                mc.setTipoMensaje(1);
//                mc.setNombreCliente(username);
//
//                mm.setCuerpo(mc);
//                return mj.objetoString(mm);
//        }
//        return "";
//    }
    public ClienteHilo getCliente(String cuerpo, Map<Integer, ClienteHilo> listadoClientes) {

        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(cuerpo);

            String msg = (String) jsonObject.get("mensaje");
            String destino = (String) jsonObject.get("destino");

            for (Map.Entry<Integer, ClienteHilo> entry : listadoClientes.entrySet()) {
                if (entry.getValue().getNombreCliente().equals(destino)) {
                    return entry.getValue();
                }
            }

            return null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public String getMsgUDP(String cuerpo, String origen) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(cuerpo);

            String msg = (String) jsonObject.get("mensaje");
            String destino = (String) jsonObject.get("destino");

            jsonObject = new JSONObject();
            jsonObject.put("mensaje", msg);

            MensajeModel mm = new MensajeModel();

            mm.setEstado(0);
            mm.setMensaje("Mensaje desde el cliente: " + origen);
            mm.setTipoOperacion(6);
            mm.setCuerpo(jsonObject.toJSONString());
            MensajeJson mj = new MensajeJson();
            return mj.objetoString(mm);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public String getMsgLlamada(String cuerpo, String origen) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObject = (JSONObject) parser.parse(cuerpo);

            String msg = (String) jsonObject.get("mensaje");
            String destino = (String) jsonObject.get("destino");

            jsonObject = new JSONObject();
            jsonObject.put("mensaje", msg);

            MensajeModel mm = new MensajeModel();

            mm.setEstado(0);
            mm.setMensaje("Iniciar llamada: " + origen);
            mm.setTipoOperacion(2);
            mm.setCuerpo(jsonObject.toJSONString());
            MensajeJson mj = new MensajeJson();
            return mj.objetoString(mm);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
    
    public String getMsgDisconnect() {
        MensajeModel mm = new MensajeModel();

        mm.setEstado(0);
        mm.setMensaje("Logout completado");
        mm.setTipoOperacion(4);
        mm.setCuerpo("Desconexión");
        MensajeJson mj = new MensajeJson();
        return mj.objetoString(mm);
    }
    
    
    /*
    Operaciones con la BD
    */
    
    public void registrarActividad(String userOrigen, String userDest, int tipo){
        LogModel model = new LogModel();
        LogDAO dao = new LogDAO();
        model.setTipo_t(tipo);
        model.setUser_or_t(userOrigen);
        model.setUser_dest_t(userDest);
        dao.insertLog(model);
    }
    
    
}
