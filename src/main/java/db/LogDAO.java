/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Samuel Ruiz
 */
public class LogDAO {

    public List<LogModel> getLogs() {
        String query = "SELECT id_log, tipo_t, usuario_or_t, user_dest_t, date_t FROM log;";

        List<LogModel> lista = new ArrayList<LogModel>();

        Connection conn = null;
        try {
            conn = DBConnection.connect();
            ResultSet rs = conn.createStatement().executeQuery(query);

            while (rs.next()) {
                LogModel lm = new LogModel();
                lm.setId_log(rs.getInt(1));
                lm.setTipo_t(rs.getInt(2));
                lm.setUser_or_t(rs.getString(3));
                lm.setUser_dest_t(rs.getString(4));
                lm.setDate_t(rs.getDate(5));

                lista.add(lm);
            }

        } catch (SQLException ex) {
            System.out.println("Error en la seleccion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }
        return lista;

    }

    public void insertLog(LogModel lm) {
        String SQL = "INSERT INTO log(tipo_t, usuario_or_t, user_dest_t, date_t) "
                + "VALUES (?, ?, ?, ?);";

        int id = 0;
        Connection conn = null;

        try {
            conn = DBConnection.connect();
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");  
            PreparedStatement pstmt = conn.prepareStatement(SQL, Statement.RETURN_GENERATED_KEYS);
           
            pstmt.setInt(1, lm.getTipo_t());
            pstmt.setString(2, lm.getUser_or_t());
            pstmt.setString(3, lm.getUser_dest_t());
            LocalDateTime localDate = LocalDateTime.now();
            pstmt.setObject(4, localDate);

            int affectedRows = pstmt.executeUpdate();
            // check the affected rows 
            if (affectedRows > 0) {
                // get the ID back
                try (ResultSet rs = pstmt.getGeneratedKeys()) {
                    if (rs.next()) {
                        id = rs.getInt(1);
                    }
                } catch (SQLException ex) {
                    System.out.println(ex.getMessage());
                }
            }
        } catch (SQLException ex) {
            System.out.println("Error en la insercion: " + ex.getMessage());
        } finally {
            try {
                conn.close();
            } catch (Exception ef) {
                System.out.println("No se pudo cerrar la conexion a BD: " + ef.getMessage());
            }
        }

        return ;
    }
}
