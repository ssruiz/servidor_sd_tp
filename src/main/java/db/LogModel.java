/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package db;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Samuel Ruiz
 */
public class LogModel {

    int id_log;
    int tipo_t;
    String user_or_t;
    String user_dest_t;
    Date date_t;

    public LogModel() {
        //asignaturas = new ArrayList<String>();
    }

    public LogModel(int id_log, int tipo_t, String user_or_t, String user_dest_t) {
        this.id_log = id_log;
        this.tipo_t = tipo_t;
        this.user_or_t = user_or_t;
        this.user_dest_t = user_dest_t;
        this.date_t = new Date();
    }

    public int getId_log() {
        return id_log;
    }

    public void setId_log(int id_log) {
        this.id_log = id_log;
    }

    public int getTipo_t() {
        return tipo_t;
    }

    public void setTipo_t(int tipo_t) {
        this.tipo_t = tipo_t;
    }

    public String getUser_or_t() {
        return user_or_t;
    }

    public void setUser_or_t(String user_or_t) {
        this.user_or_t = user_or_t;
    }

    public String getUser_dest_t() {
        return user_dest_t;
    }

    public void setUser_dest_t(String user_dest_t) {
        this.user_dest_t = user_dest_t;
    }

    public Date getDate_t() {
        return date_t;
    }

    public void setDate_t(Date date_t) {
        this.date_t = date_t;
    }

    
}
