# Sistemas Distribuidos - Trabajo Práctico  - Unidad III

## Servidor multihilo

_Trabajo práctico de la materia Sistemas Distribuidos._
_Implementación en Java de un servidor multihilo utilizando sockets TCP para llamadas y mensajes._

---
### Requerimientos de instalación 📋

```
JDK instalado.
IDE con soporte Maven(Se utilizó Netbeans).
Tener instalado Postgresql
```
---
### Instalación
```
	1. Descargar o clonar el repositorio
	2. Abir con el IDE de preferencia ( Netbeans/Eclipse/...)
	3. Actualizar y descargar las dependencias maven.
        4. Crear base de datos en postgres con el nombre de sd_tp_sr
        5. Ejecutar el script ubicado en src/data_base/database_sr.sql para crear la tabla en la base de datos.
	6. Realizar un Build.
	7. Ejecutar:
            7.1 Desde el IDE: si se usó Netbeans "Run Project" el archivo de entrada ya está configurado. En caso de usar otro IDE la clase de entrada se encuentra en el package com.servidor_sd_tp con nombre ServidorMain, ejecutar ese archivo.
		7.2 Desde una consola: situarse en la carpeta tarjet luego de realizar el build y ejecurar el .jar generado: 
		    $ java -jar servidor_sd_tp-1.0-SNAPSHOT.
```
---
## Generar Documentación 📄

#### Para generación de la documentación:
    click derecho en la raiz del proyecto > Generate Javadoc_
    Resultado en la carpeta target

## Versionado 📌

Se utilizó [Git](https://git-scm.com) para el versionado.


## Autores ✒️

* **Samuel Sebastian Ruiz Ibarra**  - [ssruiz](https://gitlab.com/ssruiz)